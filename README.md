# Custom singleuser image for use with JupyterHub

## Docker (mostly for reference)

```sh
docker build -t gitlab-registry.mpcdf.mpg.de/mpcdf/cloud/potentials/singleuser:$TAG ./docker
docker push gitlab-registry.mpcdf.mpg.de/mpcdf/cloud/potentials/singleuser:$TAG
```

## repo2docker (more flexible environment)

```sh
~/.local/bin/repo2docker --image-name gitlab-registry.mpcdf.mpg.de/mpcdf/cloud/potentials/singleuser:$TAG --user-name jovyan --user-id 1000 --no-run .
docker push gitlab-registry.mpcdf.mpg.de/mpcdf/cloud/potentials/singleuser:$TAG
```

Note that `jupyter_config.json` is included just for testing on BinderHub.  These flags are already defined in the standalone JupyterHub config.
